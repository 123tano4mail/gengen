import Foundation
import PathKit

open class AndroidStudioGen {
    private let fm = FileManager.default
    private let templatePath = Bundle.module.path(forResource: "template", ofType:nil)
    private let bundlePath:String = Path(Bundle.module.bundlePath).string
    
    let projectName: String
    let organizationId: String
    
    public init(projectName: String, organizationId: String) {
        self.projectName = projectName
        self.organizationId = organizationId
        
        copyTemplate()
        modifyTemplate()
    }
    
    public func copyTemplate() {
        if let tp = self.templatePath {
            let outputPath = bundlePath + "/LoliteApp"
            print("outputPath:\(outputPath)")
            // copy template.
            do {
                try fm.copyItem(atPath: tp, toPath: outputPath)
            } catch {
                print("failed copy template.")
                
            }
        }
    }
    
    public func modifyTemplate() {
        // read file.
        let filePath = bundlePath + "/LoliteApp"
        print("app modify path:\(filePath)")
        
        // modify settings.gradle.
        do {
            print("==== settgins.gradle ====")
            let txt = try String(contentsOfFile: filePath + "/settings.gradle", encoding: String.Encoding.utf8)
            print(txt)
            let repTxt = txt.replacingOccurrences(of: "template", with: "\(self.projectName)")
            print(repTxt)
            try repTxt.write(toFile: filePath + "/settings.gradle", atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("failed modify settings.gradle")
        }
        
        // modify app/build.gradle
        do {
            print("==== app/build.gradle ====")
            let path = filePath + "/app/build.gradle"
            let txt = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
            print(txt)
            let repTxt = txt.replacingOccurrences(of: "template", with: self.projectName)
            print(repTxt)
            try repTxt.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("failed modify app/build.gradle")
        }
        
        // modify app/src/main/java/com/example/template/MainActivity.kt
        do {
            print("==== app/src/main/java/com/example/template/MainActivity.kt ====")
            let path = filePath + "/app/src/main/java/com/example/template/MainActivity.kt"
            let txt = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
            print(txt)
            let repTxt = txt.replacingOccurrences(of: "template", with: self.projectName)
            print(repTxt)
            try repTxt.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("failed modify MainActivity.kt")
        }
        
        // modify AndroidManifest.xml
        do {
            print("==== app/src/main/AndroidManifest.xml ====")
            let path = filePath + "/app/src/main/AndroidManifest.xml"
            let txt = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
            print(txt)
            let repTxt = txt.replacingOccurrences(of: "template", with: self.projectName)
            print(repTxt)
            try repTxt.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("failed modify AndroidManifest.xml")
        }
        
        // modify app/src/androidTest/java/com/example/template/ExampleInstrumentedTest.kt
        do {
            print("==== app/src/androidTest/java/com/example/template/ExampleInstrumentedTest.kt ====")
            let path  = filePath + "/app/src/androidTest/java/com/example/template/ExampleInstrumentedTest.kt"
            let txt = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
            print(txt)
            let repTxt = txt.replacingOccurrences(of: "template", with: self.projectName)
            print(repTxt)
            try repTxt.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("failed modify ExampleInstrumenteTest.kt")
        }
        
        // modify app/test/java/com/example/template/ExampleUnitTest.kt
        do {
            print("==== app/src/test/java/com/example/template/ExampleUnitTest.kt ====")
            let path = filePath + "/app/src/test/java/com/example/template/ExampleUnitTest.kt"
            let txt = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
            print(txt)
            let repTxt = txt.replacingOccurrences(of: "template", with: self.projectName)
            print(repTxt)
            try repTxt.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("failed modify ExampleUnitTest.kt")
        }
        
        // modify /app/src/main/java/com/example/template
        do {
            let path = filePath + "/app/src/main/java/com/example"
            try fm.moveItem(atPath: path + "/template/", toPath: path + "/\(self.projectName)/")
        } catch {
            print("failed modify main/.../template directory name")
        }
        
        // modify /app/src/test/java/com/example/template
        do {
            let path = filePath + "/app/src/test/java/com/example"
            try fm.moveItem(atPath: path + "/template/", toPath: path + "/\(self.projectName)/")
        } catch {
            print("failed modify test/.../template directory name")
        }
        
        // modify /app/src/androidTest/java/com/example/template
        do {
            let path = filePath + "/app/src/androidTest/java/com/example"
            try fm.moveItem(atPath: path + "/template/", toPath: path + "/\(self.projectName)/")
        } catch {
            print("failed modify androidTest/.../template directory name")
        }
        
        // modify /app/src/main/res/values
        do {
            print("==== app/src/main/res/values/strings.xml ====")
            let path = filePath + "/app/src/main/res/values/strings.xml"
            let txt = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
            print(txt)
            let repTxt = txt.replacingOccurrences(of: "template", with: self.projectName)
            print(repTxt)
            try repTxt.write(toFile: path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("failed modify res/values/strings.xml")
        }
    }
    
    public func generate(outputPath: String) {
        print("generate command")
        let atPath = bundlePath + "/LoliteApp"
        print("atPath:\(atPath)")
        print("outputPath:\(outputPath)")
        // copy template.
        do {
            try fm.moveItem(atPath: atPath, toPath: outputPath)
        } catch {
            print("failed move project.")
            
        }
        
    }
}
