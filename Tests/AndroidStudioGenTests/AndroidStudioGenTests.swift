import XCTest
@testable import AndroidStudioGen

final class AndroidStudioGenTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        let projectName = "myapp"
        let gen = AndroidStudioGen(projectName: projectName, organizationId: "com.pentasoft")
        gen.generate(outputPath: "/Users/oyutar/Desktop/\(projectName)")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
