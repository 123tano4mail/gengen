import XCTest

import AndroidStudioGenTests

var tests = [XCTestCaseEntry]()
tests += AndroidStudioGenTests.allTests()
XCTMain(tests)
