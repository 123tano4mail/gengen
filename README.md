# AndroidStudioGen

A description of this package.

## Usage
### init
```swift
let gen = AndroidStudioGen(projectName: "myapp", organizationId: "com.example"")
```

### generate
```swift
gen.generate(outputPath: "/p/a/t/h/\(projectName)")
```
